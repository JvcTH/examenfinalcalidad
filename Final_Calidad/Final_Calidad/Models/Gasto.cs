﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_Calidad.Models
{
    public class Gasto
    {
        public int Id { get; set; }
        public int CuentaId { get; set; }

        public DateTime Fecha { get; set; }
        public double Monto { get; set; }
        public string Descripcion { get; set; }
    }
}
