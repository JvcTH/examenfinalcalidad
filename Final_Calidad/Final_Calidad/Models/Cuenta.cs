﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_Calidad.Models
{
    public class Cuenta
    {
        public int Id { get; set; }
        public String Nombre{ get; set; }
        public String Categoria { get; set; }
        public double Saldo { get; set; }
        public List<Gasto> Gastos { get; set; }
        public List<Ingreso> Ingresos { get; set; }


    }
}
