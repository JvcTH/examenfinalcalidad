﻿using Final_Calidad.Controllers;
using Final_Calidad.Models;
using Final_Calidad.Servicios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace PruebasFinal
{
    class PruebasUnitarias
    {
        [Test]
        public void pruebaGastostotales()
        {
            var cuentaMoq = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMoq.Object);
            var gastos = new List<Gasto>();
            gastos.Add(new Gasto() { Monto = 30, Descripcion = "algo", CuentaId = 1 });
            gastos.Add(new Gasto() { Monto = 40, Descripcion = "algo", CuentaId = 1 });
            gastos.Add(new Gasto() { Monto = 50, Descripcion = "algo", CuentaId = 1 });

            ICuentaService servicio = new CuentaService();
      
            Assert.AreEqual(servicio.Gastostotales(gastos),120);
        }
        [Test]
        public void pruebaIngresostotales()
        {
            var cuentaMoq = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMoq.Object);
            var ingresos = new List<Ingreso>();
            ingresos.Add(new Ingreso() { Monto = 20, Descripcion = "algo", CuentaId = 1 });
            ingresos.Add(new Ingreso() { Monto = 50, Descripcion = "algo", CuentaId = 1 });
            ingresos.Add(new Ingreso() { Monto = 10, Descripcion = "algo", CuentaId = 1 });

            ICuentaService servicio = new CuentaService();

            Assert.AreEqual(servicio.IngresosTotales(ingresos), 80);
        }
        [Test]
        public void pruebaSaldoTotal()
        {
            var cuentaMoq = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMoq.Object);
            var cuentas = new List<Cuenta>();
            cuentas.Add(new Cuenta() { Saldo = 20 });
            cuentas.Add(new Cuenta() { Saldo = 100 });
            cuentas.Add(new Cuenta() { Saldo = 90 });

            ICuentaService servicio = new CuentaService();

            Assert.AreEqual(servicio.SaldoTotal(cuentas), 210);
        }
    }
}
