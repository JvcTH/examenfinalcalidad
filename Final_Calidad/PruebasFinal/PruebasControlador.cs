﻿using Final_Calidad.Controllers;
using Final_Calidad.Models;
using Final_Calidad.Servicios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace PruebasFinal
{
    class PruebasControlador
    {
        [Test]
        public void pruebaIndex()
        {
            var cuentaMop = new Mock<ICuentaService>();
            cuentaMop.Setup(o => o.ListarCuentas()).Returns(new List<Cuenta>());
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Index() as ViewResult;
            Assert.IsInstanceOf<ViewResult>(vista);
        }
        [Test]
        public void pruebaGastos()
        {

            var cuentaMop = new Mock<ICuentaService>();
            cuentaMop.Setup(o => o.ListarGastos()).Returns(new List<Gasto>());
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Gastos() as ViewResult;
            Assert.IsInstanceOf<ViewResult>(vista);
        }

        [Test]
        public void pruebaIngresos()
        {
            var cuentaMop = new Mock<ICuentaService>();
            cuentaMop.Setup(o => o.ListarIngresos()).Returns(new List<Ingreso>());
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Ingresos() as ViewResult;
            Assert.IsInstanceOf<ViewResult>(vista);
        }
        [Test]
        public void pruebaIngresar()
        {
            var cuentaMop = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Ingresar() as ViewResult;
            Assert.IsInstanceOf<ViewResult>(vista);
        }
        [Test]
        public void pruebaPostFallidoIngresar()
        {
            var cuentaMop = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Ingresar(new Ingreso()) as ViewResult;
            Assert.IsInstanceOf<ViewResult>(vista);
        }
        [Test]
        public void pruebaPostOkIngresar()
        {
            var cuentaMop = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Ingresar(new Ingreso() { Monto = 30, Descripcion="algo",CuentaId=1 });
            Assert.IsInstanceOf<RedirectToActionResult>(vista);
        }
        [Test]
        public void pruebaPostFallidoGastar()
        {
            var cuentaMop = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Gastar(new Gasto()) as ViewResult;
            Assert.IsInstanceOf<ViewResult>(vista);
        }
        [Test]
        public void pruebaPostOkGastar()
        {
            var cuentaMop = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Gastar(new Gasto() { Monto = 30, Descripcion = "algo", CuentaId = 1 });
            Assert.IsInstanceOf<RedirectToActionResult>(vista);
        }
        [Test]
        public void pruebaGastar()
        {
            var cuentaMop = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Gastar() as ViewResult;
            Assert.IsInstanceOf<ViewResult>(vista);
        }

        [Test]
        public void pruebaCrearCuenta()
        {
            var cuentaMop = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.Crear() as ViewResult;
            Assert.IsInstanceOf<ViewResult>(vista);
        }


        [Test]
        public void pruebaPostOkCrearCuenta()
        {
            var cuentaMop = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.GuardarCuenta(new Cuenta() {Nombre="algo",Categoria="propia", Saldo = 40 }) ;
            Assert.IsInstanceOf<RedirectToActionResult>(vista);
        }
        [Test]
        public void pruebaPostFallidoCrearCuenta()
        {
            var cuentaMop = new Mock<ICuentaService>();
            var controller = new HomeController(cuentaMop.Object);
            var vista = controller.GuardarCuenta(new Cuenta() ) as ViewResult;
            Assert.IsInstanceOf<ViewResult>(vista);
        }
    }
}
